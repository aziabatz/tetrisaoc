/***************************************************************************
*   Copyright (C) 2017 by pilar   *
*   pilarb@unex.es   *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "tetris.h"

//Los métodos borrapieza, huecolibre e insertapieza sirven para evaluar si una pieza puede colocarse en una posicion

void tetris::vaciaContenedor(char * container, int w, int h)
{
   /*
    * Anchura contenedor
    * Altura contenedor
    * Area contenedor
    *
    **/
   asm volatile(
       "\n\t"
       "mov %1, %%rdx;"
       "mov %2, %%rcx;"
       "imul %%rdx,%%rcx;"
       "lea %0, %%rbx;"
       "vaciar:"
           "movq $0,(%%rbx);"
           "inc %%rbx;"
       "loop vaciar;"
       :
       :"m" (container), "m" (w), "m" (h)
       :"%rax","%rbx","%rcx","%rdx","memory"
   );

}


void tetris::borraPieza(char * container, int w, char figure[4][4], int c, int f)
{


   asm volatile(
       "\n\t"
       "lea %0,%%rsi;"//contenedor
   "lea %2,%%rdi;"//figura, direccion
   "mov %4,%%rax;"//f, fila, contador de bucle1
   "mov %1,%%rdx;"//w, anchura
   "mov $4, %%rcx;"
   "bucle1:"
       "imul %%rax,%%rdx;"
       "add %3,%%rdx;"//posCelda

       "push %%rcx;"
       "mov $4,%%rcx;"
       "bucle2:"
           "cmpq $0,(%%rdi);"//figura, datos
           "jz no_asignacion;"
           "lea (%%rsi),%%r8;"
           "lea (%%rdx),%%r9;"
           "add %%r8,%%r9;"
           "movb $0,(%%r9);"
           "no_asignacion:"
           "inc %%rdx;"
           "inc %%rdi;"
       "loop bucle2;"
       "pop %%rcx;"

   "loop bucle1;"
       //rsi,rdi,rax,rdx,rcx,
       :
       : "m" (container), "m" (w), "m" (figure), "m" (c), "m" (f)
       : "memory"

   );

}

bool tetris::huecoLibre(char * container, int w, int h, char figure[4][4], int c, int f)
{
   bool hayHueco = false;

   asm volatile(
       "\n\t"
   "lea %1,%%rbx;"/*container*/
   "lea %4,%%rsi;"/*figure*/
   "mov $1,%%r8;"/*hayhueco*/
   "mov %6,%%rax;"/*fact*/
   "mov $4,%%rcx;"
   "eval1:"
       "cmpq $1,%%r8;"
       "jnz nohayhueco;"
       "movq %5,%%rdx;"/*cact*/
       "movq %%rax,%%r9;"
       "imul %2,%%r9;"
       "add %5,%%r9;"/*poscelda*/
       "push %%rcx;"
       "movq $5,%%rcx;"
       "eval2:"
           /*condición 2o bucle*/
           "cmpq $1,%%r8;"
           "jnz nohayhueco;"

           "cmpb $0,(%%rsi);"
               "jz figure_not_zero;"
               "cmpq $0,%%rdx;"
               "jl cond;"
               "cmpq %2,%%rdx;"
               "jge cond;"
               "cmpq %3,%%rax;"
               "jge cond;"
                   "mov $0,%%r8;"
               "cond:"
                   "movq %%rbx,%%r10;"
                   "add %%r9,%%r10;"
                   "cmpq $0,(%%r10);"
                   "jz figure_not_zero;"
                       "movq $0,%%r8;"
           "figure_not_zero:"
           "inc %%rdx;"
           "inc %%r9;"
           "inc %%rsi;"
       "loop eval2;"

       "nohayhueco2:"
       "pop %%rcx;"
       "inc %%rax;"
   "loop eval1;"
   "nohayhueco:"
   "movq %%r8,%0;"
       : "=m" (hayHueco)
       : "m" (container), "m" (w), "m" (h), "m" (figure), "m" (c), "m" (f)
       : "memory"

   );

   return hayHueco;
}

void tetris::insertaPieza(char * container, int w, char figure[4][4], int c, int f)
{

   asm volatile(
       "\n\t"
   "lea %0,%%rbx;"
   "lea %2,%%rsi;"
   "mov %4,%%rax;"
   "mov $4,%%rcx;"
   "mov %1,%%rdx;"
   "inserta:"
   "imul %%rax,%%rdx;"
   "add %3,%%rdx;"
   ""
       :
       : "m" (container), "m" (w), "m" (figure), "m" (c), "m" (f)
       : "memory"

   );


}

int tetris::borraFilas(char * container, int w, int h)
{
   int nFilas=0;

   asm volatile(
       "\n\t"
   "lea %1,%%rbx;"//dirContainer
   "movq $0,%%rax;"//nFilas
   "movq %3,%%rdx;"
   "dec %%rdx;"//fAct
   "movq %%rdx,%%r8;"
   "imul %2,%%r8;"//posCelda
   "hacer:"
       "movq $0,%%r9;"//celdasOcupadas
       "movq %2,%%rcx;"
       "celdas_ocupadas:"
           "movq %%rbx,%%rsi;"
           "add %%r8,%%rsi;"//dirContainer+poscelda
           "cmpq $0,(%%rsi);"
           "jz celda_no_oc;"
           "inc %%r9;"
           "celda_no_oc:"
           "inc %%r8;"
       "loop celdas_ocupadas;"
       "cmpq %2,%%r9;"
       "jnz fila_no_completa;"
           "inc %%rax;"
           "movq %%r8,%%rdi;"
           "sub %2,%%rdi;"//posBorra
           "movq %%rdi,%%r11;"
           "sub %2,%%r11;"//posEncima
           "movq $0,%%r12;"//filaVacia
           "movq %%rdx,%%rcx;"
           "label_1:"
               "cmpq $0, %%r12;"
               "jnz fila_vacia;"
               "mov $0,%%r13;"//celdasNoVacias
               "push %%rcx;"
               "movq %2,%%rcx;"
               "label2:"
                   "movq %%rbx,%%r14;"
                   "add %%r11,%%r14;"//dirContainer+posEncima
                   "cmpq $0,(%%r14);"
                   "jz celda_noc;"
                   "inc %%r13;"
                   "celda_noc:"
                   "movq %%rbx,%%r15;"
                   "add %%rdi,%%r15;"//dirContainer+posBorra
                   "push %%rdi;"
                   "movq (%%r14),%%rdi;"
                   "movq %%rdi,(%%r15);"//[dc+pb]=[dc+pe]
                   "pop %%rdi;"
                   "inc %%rdi;"
                   "inc %%r11;"
               "loop label2;"
               "pop %%rcx;"

               "cmpq $0, %%r13;"
               "jz celdas_vacias;"
               "movq $1,%%r12;"
               "celdas_vacias:"
               "push %%rsi;"
               "movq %%rdi,%%rsi;"
               "sub %2,%%rsi;"
               "sub %2,%%rsi;"
               "movq %%rsi,%%rdi;"
               "movq %%rdi,%%rsi;"
               "sub %2,%%rsi;"
               "movq %%rsi,%%r11;"
               "pop %%rsi;"
           "loop label_1;"
           "fila_vacia:"
       "fila_no_completa:"
       "movq %2,%%r10;"
       "imul $2,%%r10;"
       "sub %%r10,%%r8;"
       "dec %%rdx;"
   "cmpq $0,%%r9;"
   "jle fin_hacer;"
   "cmpq $0,%%rdx;"
   "jl fin_hacer;"
   "jmp hacer;"
   "fin_hacer:"
   //nfilas devolver
   "movq %%rax,%0;"

       : "=m" (nFilas)
       : "m" (container), "m" (w), "m" (h)
       : "memory","rax","rbx","rcx","rdx","r8","r9","r10","r11","r12","r13","r14","r15"

   );

   return nFilas;
}

void tetris::annadeFila(char * container, int w, int h, char * row)
{
   asm volatile(
       "\n\t"
   "lea %0,%%rbx;"
   "mov $0,%%rax;"//posCelda
   "mov %1,%%rdx;"//posCopia
   "mov %2,%%rcx;"
   "dec %%rcx;"
   "bucle_uno:"
       "push %%rcx;"
       "mov %1,%%rcx;"
       "bucle_dos:"
       "mov %%rbx,%%rsi;"
       "add %%rdx,%%rsi;"
       "mov (%%rsi),%%rdi;"//dir(container+poscopia)
       "add %%rax,%%rbx;"//dir(container+poscelda)
           "mov (%%rdi),%%r8;"
           "mov %%r8,(%%rbx);"
       "inc %%rax;"
       "inc %%rdx;"
       "loop bucle_dos;"
       "pop %%rcx;"
   "loop bucle_uno;"
   "lea %3,%%rsi;"
   "mov %1,%%rcx;"
   "lea %0,%%rbx;"
   "bucle_tres:"
   "add %%rax,%%rbx;"
   "mov (%%rsi),%%rdi;"
   "mov %%rdi,%%rbx;"
   "inc %%rax;"
   "inc %%rsi;"
   "loop bucle_tres;"
       :
       : "m" (container), "m" (w), "m" (h), "m" (row)
       : "memory"

   );

}
